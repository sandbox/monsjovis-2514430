<?php

function ga_views_load_sdk() {
  $info = libraries_load('google-api-php-client');
  if (!$info['loaded']) {
    drupal_set_message(t("Can't authenticate with google as library is missing check Status report or Readme for requirements"), 'error');
    return FALSE;
  }
  require_once DRUPAL_ROOT . '/' . $info['library path'] . '/src/Google/Service/Analytics.php';
  return TRUE;
}


/**
 * pull data from a source
 * @param $source_name : then name of the source from which to pull data
 * @param $metrics : an array or string of the metrics to pull
 * @param $time_frame : a time_frame obj 
 * @return: an array of obj ready for the ga_views_count table 
 */
function ga_views_get_data($metric, $start_time, $timeframe='', $filter=NULL, $redirects=array()) {

  $counts = array();

  foreach (ga_views_ga_data($metric, $start_time, $filter) as $result) {

    $count = new stdClass;
    $count->path = $result[0];
    $count->system_path = NULL;
    $count->count = intval($result[1]);
    $count->metric = $metric;
    $count->entity_id = NULL;
    $count->entity_type = NULL;
    $count->timeframe = $timeframe;

    if (strpos($count->path, '/') !== FALSE) {
      $count->path = substr($count->path, 1);
    }
    if (strpos($count->path, '?')  !== FALSE) {
      $count->path = substr($count->path, 0, strpos($count->path, '?'));
    }

    $count->system_path = drupal_lookup_path('source', $count->path);
    if (empty($count->path) && empty($count->system_path)) {
      $count->system_path = '<front>';
    }

    if (!empty($count->path) && array_key_exists($count->path, $redirects)) {
      $count->system_path = $redirects[$count->path];
    }

    if (!empty($count->system_path)) {
      if (preg_match('/^node\/([0-9]*)/', $count->system_path, $matches)  ) {
        $count->entity_id = $matches[1];
        $count->entity_type = 'node';
      } elseif (preg_match('/^taxonomy\/term\/([0-9]*)/', $count->system_path, $matches)  ) {
        $count->entity_id = $matches[1];
        $count->entity_type = 'taxonomy_term';
      }
    }

    $key = (!empty($count->system_path) ? $count->system_path : $count->path).'|'.$count->metric.'|'.$count->timeframe;
    if (isset($counts[$key])) {
      $counts[$key]->count += $count->count;
    } else {
      $counts[$key] = $count;
    }

  }

  return $counts;
}


/**
 *  call back funciton for pulling data from google anaylics
 *  @param metrics_in : and array or string of the metrics to pull
 *  @param start_date : time as an int
 *  @return : and array of external_statistics_count db obj
 *
 */
function ga_views_ga_data($metrics, $start_date = 0, $filter=NULL) {
  if (!is_array($metrics)) {
    $metrics = array($metrics);
  }
  $request['dimensions'] = array('ga:pagePath');
  $request['metrics'] = $metrics;
  
  if ($start_date) {
    $request['start_date'] = date('Y-m-d', $start_date);
  } else {
    $request['start_date'] = NULL;
  }
  
  $request['end_date'] = date('Y-m-d', time());
  $request['sort_metric'] = "-" . $metrics[0];
  $request['filter'] = $filter;
  
  $data = ga_views_query_data($request);

  return $data;
}


/*
 * Helper function to list metrics with plugin names and google analytics names
 */
function ga_views_ga_metrics($all=FALSE) {
  $metrics = array(
    'pageviews' => 'Page Views',
    'uniquePageviews' => 'Unique Page Views',
    'avgTimeOnPage' => 'Average Time on Page'
  );
  if (!$all) {
    $enabled = variable_get('ga_views_enabled_metrics', array('pageviews'));
    foreach ($metrics as $k => $v) {
      if (!isset($enabled[$k]) || !$enabled[$k]) {
        unset($metrics[$k]);
      }
    }
  }
  return $metrics;
}


/**
 *  @param $just_keys : whether we should return all the data or just the keys->values array
 */
function ga_views_ga_timeframes($just_keys=FALSE, $all=FALSE) {
  if ($just_keys) {
    $timeframes = array(
      'hour' => 'in the past hour',
      'today' => 'in the past 24 hours',
      '2days' => 'in the past 48 hours',
      'week' => 'in the past 7 days',
      'month' => 'in the past 31 days',
      'forever' => 'for all time'
    );
  }
  else {
    $timeframes = array(
      'hour' => array('secsToSub' => (60*60), 'filter' => NULL, 'title' => 'in the past hour'),
      'today' => array('secsToSub' => (60*60*24), 'filter' => NULL, 'title' => 'in the past 24 hours'),
      '2days' => array('secsToSub' => (60*60*24*2), 'filter' => NULL, 'title' => 'in the past 48 hours'),
      'week' => array('secsToSub' => (60*60*24*7), 'filter' => NULL, 'title' => 'in the past 7 days'),
      'month' => array('secsToSub' => (60*60*24*31), 'filter' => NULL, 'title' => 'in the past 31 days'),
      'forever' => array('secsToSub' => time() - strtotime('2005-01-01'), 'filter' => NULL, 'title' => 'for all time')
    );
  }
  
  if (!$all) {
    $enabled = variable_get('ga_views_enabled_timeframes', array('today', 'month'));
    foreach ($timeframes as $k => $v) {
      if (!isset($enabled[$k]) || !$enabled[$k]) {
        unset($timeframes[$k]);
      }
    }
  }
  
  return $timeframes;
}


function ga_views_client_exists() {
  return gauth_account_is_authenticated('ga_views', TRUE);
}


function ga_views_get_client() {
  if (ga_views_client_exists()) {
    return gauth_client_get('ga_views', TRUE);
  }
  return gauth_account_authenticate('ga_views', TRUE);
}


function ga_views_ga_get_profiles() {
  ga_views_load_sdk();
  $client = ga_views_get_client();
  $analytics = new Google_Service_Analytics($client);

  $profiles = array();  
  $accounts = array();

  foreach ($analytics->management_accounts->listManagementAccounts()->getItems() as $account) {
    $accounts[$account->id] = $account->name;
  }

  foreach ($analytics->management_profiles->listManagementProfiles('~all', '~all')->getItems() as $profile) {
    $profiles[$profile->id] = $accounts[$profile->accountId] . ' - ' . $profile->name . ' ('.$profile->id.')';
  }

  return $profiles;
}


function ga_views_query_data($request) {
  
  $profileId = variable_get('ga_views_profile', '');
  $page = 0;
  $rows = array();

  ga_views_load_sdk();
  $client = ga_views_get_client();
  $analytics = new Google_Service_Analytics($client);  

  do {

    $page++;

    $optParams = array(
      'dimensions' => implode(',', $request['dimensions']),
      'sort' => 'ga:pagepath',
      'start-index' => 1 + ($page-1)*GA_VIEWS_MAX_RESULTS,
      'max-results' => GA_VIEWS_MAX_RESULTS
    );
    $results = $analytics->data_ga->get(
      'ga:' . $profileId,
      $request['start_date'], 
      $request['end_date'],
      'ga:pageviews',
      $optParams
    );

    $rows = array_merge($rows, $results->getRows());

  } while ($results->getNextLink() != NULL);

  return $rows;

  /*
  if ($user && $password) {
    try{
      $ga = new gapi($user, $password, NULL, $type);
      $data = $ga->requestReportData($aid, $request['dimensions'], $request['metrics'], $request['sort_metric'], NULL, $request['start_date'], $request['end_date'], 1, GA_VIEWS_MAX_RESULTS);
    } catch (Exception $e) {
      drupal_set_message(t('Invalid Google Analytics login.'), 'error');
      watchdog('ga_views', 'Invalid Google Analytics login.');
      return array();
    }    
    return $data;    
  }
  else {
    drupal_set_message(t('Google Analytics Email and password not set.'), 'error');
    watchdog('ga_views', 'Google Analytics email and password not set.');
  }*/

}


function ga_views_update_counts() {

  $startTime = microtime(true);
  $data = array();
  
  $metrics = ga_views_ga_metrics();
  $timeframes = ga_views_ga_timeframes();
  $now = time();

  $redirects = array();
  if (function_exists('redirect_load_multiple')) {
    foreach(redirect_load_multiple(false) as $entity) {
        $redirects[$entity->source] = $entity->redirect;
    }
  }

  foreach ($metrics as $metric => $title) {
    foreach ($timeframes as $key => $time) {
      $new_data = ga_views_get_data($metric, $now-$time['secsToSub'], $key, $time['filter'], $redirects);
      $data = array_merge($data, $new_data);
    }
  }

  $dataPieces = array();
  $queryLength = 800;
  for ($i = 0; $i < intval(count($data)/$queryLength)+1; $i++) {
    $dataPieces[] = array_slice($data, $i * $queryLength, $queryLength);
  }

  foreach ($dataPieces as $items) {
    $delConditions = db_or();
    $query = db_insert('ga_views')->fields(array('path','system_path','count','metric','entity_id','entity_type','timeframe'));
    $delQuery = db_delete('ga_views');
    foreach ($items as $row) {
      $query->values(array(
        'path'=>$row->path,
        'system_path'=>$row->system_path,
        'count'=>$row->count,
        'metric'=>$row->metric,
        'entity_id'=>$row->entity_id,
        'entity_type'=>$row->entity_type,
        'timeframe'=>$row->timeframe
      ));
      $delConditions->condition(db_and()
        ->condition(!empty($row->system_path) ? 'system_path' : 'path', !empty($row->system_path) ? $row->system_path : $row->path, '=')
        ->condition('metric', $row->metric, '=')
        ->condition('timeframe', $row->timeframe, '=')
      );
    }
    $delQuery->condition($delConditions);
    $delQuery->execute();
    $query->execute();
  }

  drupal_set_message(t('Counts Successfully Updated'));
  watchdog('ga_views', t('Counts Successfully Updated in @time sec (@count  records)'), array('@count'=>count($data), '@time'=>round(microtime(true)-$startTime), 2), WATCHDOG_INFO);

  return $data;
}