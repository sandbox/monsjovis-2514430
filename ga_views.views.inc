<?php 
/**
 *  Implementation of hook_views_data
 *
 *  Exposing the count table to views as a field, a filter and a sort
 */
function ga_views_views_data() {
  $data = array();
    
  foreach(ga_views_ga_metrics(true) as $metricKey=>$metricName){

    $metricKeyLc = strtolower($metricKey); 
    
    foreach(ga_views_ga_timeframes(true) as $timeframeKey=>$timeframeName) {
      $data['ga_views_'.$metricKeyLc.'_'.$timeframeKey]['table']['group']  = t('Statistics');
      $data['ga_views_'.$metricKeyLc.'_'.$timeframeKey]['table']['join']  = array(
        'node' => array(
          'table' => 'ga_views',
          'left_field' => 'nid',          
          'field' => 'entity_id',
          'type' => 'LEFT OUTER',
          'extra' => array(
            array('field' => 'entity_type', 'value' => 'node'),
            array('field' => 'metric', 'value' => $metricKey),
            array('field' => 'timeframe', 'value' => $timeframeKey),
          )
        ),
        'taxonomy_term' => array(
          'table' => 'ga_views',
          'left_field' => 'tid',
          'field' => 'entity_id',
          'type' => 'LEFT OUTER',
          'extra' => array(
            array('field' => 'entity_type', 'value' => 'taxonomy_term'),
            array('field' => 'metric', 'value' => $metricKey),
            array('field' => 'timeframe', 'value' => $timeframeKey),
          )
        )
      );
      $data['ga_views_'.$metricKeyLc.'_'.$timeframeKey]['count'] = array(
        'title' => t("$metricName $timeframeName"),
        'help' => t('Information retrieved from Google Analytics'),
        'field' => array(
          'handler'=> 'views_handler_field_numeric',
          'click sortable' => TRUE,
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_numeric',
        )
      );
    }
  }
   
  return $data;
}