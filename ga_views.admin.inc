<?php

/*
 * build the admin form
 */
function ga_views_admin_settings() {
  $form = array();
  $options = array_replace(array(-1 => '- ' . t('Select') . ' -'), ga_views_ga_get_profiles());
  $form['ga_views_account'] = array(
    '#type' => 'fieldset',
    '#title' => t('Google Analytics Tracking Account'),
    '#description' => t('The authentication works with the GAuth Module. There must be an account named "ga_views" with enabled Google Analytics service.'),
  );
  if (count($options) > 0) {
    $form['ga_views_account']['ga_views_profile'] = array(
      '#type' => 'select',
      '#title' => t('Google Analytics Profile to Use'),
      '#description' => t('The Google Analytics profile from which to retrieve statistics'),
      '#options' => $options,
      '#default_value' => variable_get('ga_views_profile', '')
    );    
  }
  else {
    $form['ga_views_account']['ga_views_profile'] = array(
      '#type' => 'markup',
      '#markup' => '<div class="messages warning">' . t('Could not load your profiles. Did you authenticate with gauth?') . '</div>'
    );
  }
  
  $form['enabled_stats'] = array(
    '#type' => 'fieldset',
    '#title' => t('Enabled Statistics'),
    '#description' => t('Make sure to clear the Drupal cache after changing this setting in order to inform Views of the new settings. <br/><em><b>WARNING:</b> Do not disable a setting which is currently in use in Views.</em>')
  );
  
  $form['enabled_stats']['ga_views_enabled_metrics'] = array(
    '#type' => 'checkboxes',
    '#default_value' => variable_get('ga_views_enabled_metrics', array('pageviews')),
    '#options' => ga_views_ga_metrics(true),
    '#title' => t('Metrics'),
    '#description' => t('The metrics that will be available from Google Analytics in Views.')
  );
  
  $form['enabled_stats']['ga_views_enabled_timeframes'] = array(
    '#type' => 'checkboxes',
    '#default_value' => variable_get('ga_views_enabled_timeframes', array('today', 'month')),
    '#options' => ga_views_ga_timeframes(TRUE, TRUE),
    '#title' => t('Time Frames'),
    '#description' => t('The timeframes that will be available from Google Analytics in Views.')
  );
  
  $form['cron'] = array(
    '#type' => 'fieldset',
    '#title' => t('Cronjob'),
    '#description' => variable_get('ga_views_next_cron_run', 0) && variable_get('ga_views_cron_import', TRUE) ? t('Next cron import at @time', array('@time'=>format_date(variable_get('ga_views_next_cron_run', 0)))) : '',
  );
  $form['cron']['ga_views_cron_import'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Cronjob'),
    '#description' => t('Enable automatic imports with the cron job'),
    '#default_value' => variable_get('ga_views_cron_import', TRUE),
  );  
  $form['cron']['ga_views_cron_times'] = array(
    '#type' => 'textarea',
    '#title' => t('Cron times'),
    '#description' => t('Times when the import has to be done. It has to be parseable with strtotime(). For more times add each one on a new line.'),
    '#default_value' => variable_get('ga_views_cron_times', '')
  );
  
  if (variable_get('ga_views_profile', FALSE)) {
    $form['actions']['ga_views_update'] = array(
      '#type' => 'button',
      '#value' => t('Update Counts'),
      '#weight' => 20,
      '#executes_submit_callback' => TRUE,
      '#submit' => array('ga_views_update_counts'),
    );
  }

  return system_settings_form($form);
}